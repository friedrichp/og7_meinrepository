package fibonacci;

public class Fibonacci {

	public static int fibo(int eingabe) {
	
		if (eingabe==0)
			return 0;
		if (eingabe==1)
			return 1;
		if (eingabe>1)
			return (fibo(eingabe-2)+fibo(eingabe-1));
		else {
			System.out.println("Fehler");
			return 0;
		}
	}
}
