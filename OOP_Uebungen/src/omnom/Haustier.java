package omnom;

public class Haustier {
	
	private String name;
	private int hunger;
	private int muede;
	private int zufrieden;
	private int gesund;
	
	public Haustier(String string) {
		this.name = string;
		this.hunger = 100;
		this.muede = 100;
		this.zufrieden = 100;
		this.gesund = 100;
		
	}
	
	public int getHunger() {
		return hunger;
	}

	public void setHunger(int hunger) {
		this.hunger = hunger;
		if (this.hunger>100) {
			this.hunger = 100;
		}
		if (this.hunger<0) this.hunger = 0;
	}

	public int getMuede() {
		return muede;
	}

	public void setMuede(int muede) {
		this.muede = muede;
		if (this.muede>100) {
			this.muede = 100;
		}
		if (this.muede<0) this.muede = 0;
	}

	public int getZufrieden() {
		return zufrieden;
	}

	public void setZufrieden(int zufrieden) {
		this.zufrieden = zufrieden;
		if (this.zufrieden>100) {
			this.zufrieden = 100;
		}
		if (this.zufrieden<0) this.zufrieden = 0;
	}

	public int getGesund() {
		return gesund;
	}

	public void setGesund(int gesund) {
		this.gesund = gesund;
		if (this.gesund>100) {
			this.gesund = 100;
		}
		if (this.gesund<0) this.gesund = 0;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public void fuettern(int i) {
		this.setHunger(this.getHunger() + i);
	}

	public void schlafen(int i) {
		this.setMuede(this.getMuede() + i);
	}

	public void spielen(int i) {
		this.setZufrieden(this.getZufrieden() + i);
		
	}

	public void heilen() {
		this.setGesund(100);
		
	}
	
}