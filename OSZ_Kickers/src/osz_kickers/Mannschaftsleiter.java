package osz_kickers;

public class Mannschaftsleiter extends Spieler {

	private String nameMannschaft;
	private int rabatt;
	
	public Mannschaftsleiter() {
		super();
	}
	
	public Mannschaftsleiter(String name, String telefonnummer, boolean jahresbeitragBezahlt, int trikotnummer,
			String spielposition, String nameMannschaft, int rabatt) {
		super(name, telefonnummer, jahresbeitragBezahlt, trikotnummer, spielposition);
		this.nameMannschaft = nameMannschaft;
		this.rabatt = rabatt;
	}
	
	public String getNameMannschaft() {
		return nameMannschaft;
	}
	public void setNameMannschaft(String nameMannschaft) {
		this.nameMannschaft = nameMannschaft;
	}
	public int getRabatt() {
		return rabatt;
	}
	public void setRabatt(int rabatt) {
		this.rabatt = rabatt;
	}
	
}
