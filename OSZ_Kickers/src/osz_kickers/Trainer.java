package osz_kickers;

public class Trainer extends Person {

	private char lizenzklasse;
	private int aufwandentschaedigung;
	
	public Trainer() {
		super();
	}

	public Trainer(String name, String telefonnummer, boolean jahresbeitragBezahlt, char lizenzklasse,
			int aufwandentschaedigung) {
		super(name, telefonnummer, jahresbeitragBezahlt);
		this.lizenzklasse = lizenzklasse;
		this.aufwandentschaedigung = aufwandentschaedigung;
	}

	public char getLizenzklasse() {
		return lizenzklasse;
	}

	public void setLizenzklasse(char lizenzklasse) {
		this.lizenzklasse = lizenzklasse;
	}

	public int getAufwandentschaedigung() {
		return aufwandentschaedigung;
	}

	public void setAufwandentschaedigung(int aufwandentschaedigung) {
		this.aufwandentschaedigung = aufwandentschaedigung;
	}
	
}
