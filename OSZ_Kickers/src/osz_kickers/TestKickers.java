package osz_kickers;

public class TestKickers {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Spieler testSpieler = new Spieler("Peter", "+49 123456789", true, 13, "Tor");
		Mannschaftsleiter testMannschaftsleiter = new Mannschaftsleiter("Die Mannschaftsleiter (nicht 'der')", "0900 123456789 (5€/Sekunde aus dem deutschen Festnetz)", false, 1, "Angriff", "Die Leiterfreunde", 50);
		Trainer testTrainer = new Trainer("Trainer Thomas", "Ich habe nur eine E-Brief-Adresse!", false, 'A', 450);
		Schiedsrichter testSchiedsrichter = new Schiedsrichter("Schiedsrichter Schulz (Martin Schulz)", "Seit meiner Wahlniederlage rede ich nicht mehr mit Menschen.", false, 773);
		Person testPerson = new Person("Tim der tagtägliche Toilettentieftaucher", "Ist ihre Toilette auch verstopft? Dann melden Sie sich auf der Website: www.facebookcorewwwi.onion", false);
		
		testSpieler.setName("Lars Peter");
		
		System.out.println(testSpieler.getName() + "; " + testSpieler.getTelefonnummer() + "; " + testSpieler.isJahresbeitragBezahlt() + "; " + testSpieler.getTrikotnummer() + "; " + testSpieler.getSpielposition());
		System.out.println(testMannschaftsleiter.getName() + "; " + testMannschaftsleiter.getTelefonnummer() + "; " + testMannschaftsleiter.isJahresbeitragBezahlt() + "; " + testMannschaftsleiter.getTrikotnummer() + "; " + testMannschaftsleiter.getSpielposition() + "; " + testMannschaftsleiter.getNameMannschaft() + "; " + testMannschaftsleiter.getRabatt());
		System.out.println(testTrainer.getName() + "; " + testTrainer.getTelefonnummer() + "; " + testTrainer.isJahresbeitragBezahlt() + "; " + testTrainer.getLizenzklasse() + "; " + testTrainer.getAufwandentschaedigung());
		System.out.println(testSchiedsrichter.getName() + "; " + testSchiedsrichter.getTelefonnummer() + "; " + testSchiedsrichter.isJahresbeitragBezahlt() + "; " + testSchiedsrichter.getAnzahlGepfiffeneSpiele());
		System.out.println(testPerson.getName() + "; " + testPerson.getTelefonnummer() + "; " + testPerson.isJahresbeitragBezahlt());
	}

}
