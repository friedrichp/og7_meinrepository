package osz_kickers;

public class Schiedsrichter extends Person {

	private int anzahlGepfiffeneSpiele;

	public Schiedsrichter() {
		super();
	}

	public Schiedsrichter(String name, String telefonnummer, boolean jahresbeitragBezahlt, int anzahlGepfiffeneSpiele) {
		super(name, telefonnummer, jahresbeitragBezahlt);
		this.anzahlGepfiffeneSpiele = anzahlGepfiffeneSpiele;
	}

	public int getAnzahlGepfiffeneSpiele() {
		return anzahlGepfiffeneSpiele;
	}

	public void setAnzahlGepfiffeneSpiele(int anzahlGepfiffeneSpiele) {
		this.anzahlGepfiffeneSpiele = anzahlGepfiffeneSpiele;
	}
	
}
