package osz_kickers;

public class Spieler extends Person {

	private int trikotnummer;
	private String Spielposition;
	
	public Spieler() {
		super();
	}
	
	public Spieler(String name, String telefonnummer, boolean jahresbeitragBezahlt, int trikotnummer,
			String spielposition) {
		super(name, telefonnummer, jahresbeitragBezahlt);
		this.trikotnummer = trikotnummer;
		Spielposition = spielposition;
	}

	public int getTrikotnummer() {
		return trikotnummer;
	}

	public void setTrikotnummer(int trikotnummer) {
		this.trikotnummer = trikotnummer;
	}

	public String getSpielposition() {
		return Spielposition;
	}

	public void setSpielposition(String spielposition) {
		Spielposition = spielposition;
	}
	
}
