package primzahlen;

public class Stoppuhr {

	private long start;
	private long stopp;
	
	public void start() {
		start = System.currentTimeMillis();
	}
	
	public void stopp() {
		stopp = System.currentTimeMillis();
	}
	
	public void reset() {
		start = 0;
		stopp = 0;
	}
	
	public long getMc() {
		return (stopp - start);
	}
}
