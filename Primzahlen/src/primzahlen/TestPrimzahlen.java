package primzahlen;

import java.util.Scanner;

public class TestPrimzahlen {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		Scanner input = new Scanner(System.in);
		Stoppuhr testStoppuhr = new Stoppuhr();
		
		
		int s = input.nextInt();
		testStoppuhr.start();
		
		for (int i = (int) Math.pow(10, (s - 1)); i < Math.pow(10, s); i++) {
			if (Primzahlen.isPrim(i) == true) {
				System.out.println(i);
				break;
			}
		}
		
		testStoppuhr.stopp();
		
		System.out.println("[Zeit]: " + testStoppuhr.getMc());
	}

}
