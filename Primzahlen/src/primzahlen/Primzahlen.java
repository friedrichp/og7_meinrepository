package primzahlen;

public class Primzahlen {

	public static boolean isPrim(long eingabe) {
		for (long i = (eingabe - 1); i>1; i--) {
			if (eingabe % i == 0) {
				return false;
			}
		}
		return true;
	}
}
