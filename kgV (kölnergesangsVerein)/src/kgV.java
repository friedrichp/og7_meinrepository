
public class kgV {

	public static String kgV(int zahl1, int zahl2) {
		boolean ende = false;
		int zs1 = zahl1;
		int zs2 = zahl2;
		int ergebnis = -1;
		if (zahl1 == 0 || zahl2 == 0) {
			ergebnis = 0;
			return "0";
		}else if (zahl1 < 0 || zahl2 < 0){
			return "Fehler";
		}else {
			do {
				if (zs1<=zs2) {
					if(zs1 % zahl1 == 0 && zs1 % zahl2 == 0) {
						ende = true;
						ergebnis = zs1;
					}else zs1 = ((zs1/zahl1)+1)*zahl1;
				}else {
					if(zs2 % zahl1 == 0 && zs2 % zahl2 == 0) {
						ende = true;
						ergebnis = zs2;
					}else zs2 = ((zs2/zahl2)+1)*zahl2;
				}
			} while (ende == false);
			return Integer.toString(ergebnis);
		}
	}
}
