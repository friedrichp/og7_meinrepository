import static org.junit.jupiter.api.Assertions.*;

import org.junit.jupiter.api.Test;

class GesangsTest {

	@Test
	void test() {
		assertEquals("0", kgV.kgV(0, 1));
		assertEquals("2", kgV.kgV(2, 2));
		assertEquals("2", kgV.kgV(1, 2));
		//assertEquals("0", kgV.kgV(97, 103));
		assertEquals("Fehler", kgV.kgV(-6, 8));
		assertEquals("0", kgV.kgV(0, 0));
		assertEquals("9", kgV.kgV(3, 9));
		assertEquals("600", kgV.kgV(600, 3));
		assertEquals("2100000000", kgV.kgV(2100000000, 1));
		assertEquals("Fehler", kgV.kgV(-6, -6));
	}

}
