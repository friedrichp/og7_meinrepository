package keyStore;

import java.util.Arrays;

public class KeyStore01 {

	String[] keys;

	public KeyStore01() {
		keys = new String[100];
	}
	
	public KeyStore01(int length) {
		keys = new String[length];
	}
	
	public boolean add(String eingabe) {
		for (int i = 0; i < keys.length; i++) {
			if(keys[i] == null) {
				keys[i] = eingabe;
				return true;
			}
		}
		return false;
	}

	public int indexOf(String eintrag) {
		for (int i = 0; i < keys.length; i++) {
			if (keys[i] != null) {
				if (keys[i].equals(eintrag)) {
					return i;
				}	
			}
		}
		return -1;
	}

	public void remove(int index) {
		keys[index] = null;
	}

	@Override
	public String toString() {
		return "KeyStore01 [keys=" + Arrays.toString(keys) + "]";
	}

	
	
}
