package keyStore;

import java.util.ArrayList;

public class KeyStore02 {

	ArrayList<String> keys = new ArrayList<String>();
	
	public KeyStore02() {
		
	}
	
	public boolean add(String eingabe) {
		keys.add(eingabe);
		return true;
	}

	public int indexOf(String eintrag) {
		for (int i = 0; i < keys.size(); i++) {
			if (keys.get(i) != null) {
				if (keys.get(i).equals(eintrag)) {
					return i;
				}	
			}
		}
		return -1;
	}

	public void remove(int index) {
		keys.remove(index);
		//keys.set(index, null);
	}

	@Override
	public String toString() {
		return "KeyStore02 [keys=" + keys + "]";
	}
	
}