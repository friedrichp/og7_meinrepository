package buecherliste;

import java.util.*;

public class CollectionTest {
	
	static void menue() {
		System.out.println("\n ***** Buch-Verwaltung *******");
		System.out.println(" 1) eintragen ");
		System.out.println(" 2) finden ");
		System.out.println(" 3) l�schen");
		System.out.println(" 4) Die gr��te ISBN");
		System.out.println(" 5) zeigen");
		System.out.println(" 9) Beenden");
		System.out.println(" ********************");
		System.out.print(" Bitte die Auswahl treffen: ");
	} // menue

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		List<Buch> buchliste = new LinkedList<Buch>();
        Scanner myScanner = new Scanner(System.in);

		char wahl;
		String eintrag;
		
		int index;
		do {
			menue();
			wahl = myScanner.next().charAt(0);
			switch (wahl) {
			case '1':	
				System.out.println("<Autor> <Titel> <ISBN>");
				buchliste.add(new Buch(myScanner.next(), myScanner.next(), myScanner.next()));
				break;
			case '2':
				System.out.println("Gib mal ISBN: ");
				String eingabe = myScanner.next();
				for (int i = 0; i < buchliste.size(); i++) {
					if (buchliste.get(i).getIsbn().equals(eingabe) == true) {
						System.out.println("Das Buch ist an Stelle " + i);
						break;
					}
				}
				break;
			case '3':
				buchliste.remove(myScanner.next());
				break;
			case '4':
				boolean vertauschen;
				do {
					vertauschen = true;
					for (int i = 0; i < buchliste.size() -1; i++) {
						if (buchliste.get(i).compareTo(buchliste.get(i+1)) > 0) {
							Buch zwischenspeicher = buchliste.get(i);
							buchliste.set(i, buchliste.get(i+1));
							buchliste.set(i+1, zwischenspeicher);
							vertauschen = false;
						}
					}
				}while(vertauschen == false);
				System.out.println(buchliste.get(0));
				break;		
			case '5':
				for (int i = 0; i < buchliste.size(); i++) {
					System.out.println(buchliste.get(i).toString());
				}
				break;
			case '9':
				System.exit(0);
				break;
			default:
				menue();
				wahl = myScanner.next().charAt(0);
		} // switch

	} while (wahl != 9);
}// main

	public static Buch findeBuch(List<Buch> buchliste, String isbn) {
		// add your implementation
		return null;
	}

//	public static boolean loescheBuch(........) {
		// add your implementation
//	}

//	public static String ermitteleGroessteISBN(........) {
		// add your implementation
//	}

}
