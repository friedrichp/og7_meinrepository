package bubblesort;

import java.util.Scanner;

public class Bubblesort {

	public static void main(String[] args) {
		Scanner input = new Scanner(System.in);
		int[] intArray = new int[10];
		for (int i = 0; i<intArray.length; i++) {
			System.out.print("[Zahl" + (i+1) + "]: ");
			intArray[i] = input.nextInt();
		}
		intArray = bubbleSort(intArray);
		for (int i = 0; i<intArray.length; i++) {
			System.out.print(intArray[i] + ", ");
		}
	}
	
	public static int[] bubbleSort(int[] intArray) {
		boolean vertauscht;
		do {
			vertauscht = false;
			for (int i = 0; i<intArray.length-1; i++) {
				if (intArray[i]>intArray[i+1]) {
					int zwischenspeicher = intArray[i];
					intArray[i] = intArray[i+1];
					intArray[i+1] = zwischenspeicher;
					vertauscht = true;
				}
			}
		}while (vertauscht == true);
		return intArray;
	}
}
