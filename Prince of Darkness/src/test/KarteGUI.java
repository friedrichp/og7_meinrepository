package test;

import java.awt.BorderLayout;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Toolkit;

import javax.swing.JFrame;
import javax.swing.WindowConstants;

import karte.Held;
import karte.Karte;
import javax.swing.JPanel;
import javax.swing.JLabel;
import java.awt.GridLayout;
import javax.swing.SwingConstants;

/**
  *
  * GUI-Klasse zum testweisen Anzeigen einer Karte
  *
  * @version 1.0 from 19.10.2012
  * @author Tenbusch
  */

public class KarteGUI extends JFrame {
  // Anfang Attribute
  Held h = new Held("Kitty", "D�mon", "Mistvieh", 25, 75, 50, 50);
  Karte k = new Karte(h);
  // Ende Attribute

  public KarteGUI (String title) {
    super (title);
    setDefaultCloseOperation(WindowConstants.DISPOSE_ON_CLOSE);
    int frameWidth = 300; 
    int frameHeight = 500;
    setSize(frameWidth, frameHeight);
    Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
    int x = (d.width - getSize().width) / 2;
    int y = (d.height - getSize().height) / 2;
    setLocation(x, y);
    Container cp = getContentPane();
    cp.setLayout(new BorderLayout());
    cp.add(k, BorderLayout.CENTER);
    
    JPanel pnl_North = new JPanel();
    getContentPane().add(pnl_North, BorderLayout.NORTH);
    pnl_North.setLayout(new GridLayout(0, 2, 0, 0));
    
    JLabel lbl_Name = new JLabel("Heldname");
    pnl_North.add(lbl_Name);
    
    JLabel lbl_Typ = new JLabel("Typ");
    lbl_Typ.setHorizontalAlignment(SwingConstants.TRAILING);
    pnl_North.add(lbl_Typ);
    
    JPanel pnl_South = new JPanel();
    getContentPane().add(pnl_South, BorderLayout.SOUTH);
    pnl_South.setLayout(new BorderLayout(0, 0));
    
    JLabel lbl_Beschreibung = new JLabel("Beschreibung");
    pnl_South.add(lbl_Beschreibung, BorderLayout.NORTH);
    
    JPanel panel = new JPanel();
    pnl_South.add(panel, BorderLayout.CENTER);
    panel.setLayout(new GridLayout(4, 1, 0, 0));
    
    JLabel lbl_Leben = new JLabel("Leben");
    panel.add(lbl_Leben);
    
    JLabel lbl_Ruestung = new JLabel("R\u00FCstung");
    panel.add(lbl_Ruestung);
    
    JLabel lbl_Magieresistenz = new JLabel("Magieresistenz");
    panel.add(lbl_Magieresistenz);
    
    JLabel lblNewLabel_3 = new JLabel("Angriff");
    panel.add(lblNewLabel_3);
    
    JPanel pnl_Center = new JPanel();
    getContentPane().add(pnl_Center, BorderLayout.CENTER);
    // Anfang Komponenten
    // Ende Komponenten
    setResizable(true);
    setVisible(true);
  }

  // Anfang Methoden
  // Ende Methoden

  public static void main(String[] args) {
    new KarteGUI("KarteGUI");
  }
}
