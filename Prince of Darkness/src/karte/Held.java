package karte;

public class Held {

	String name, typ, beschreibung, bildpfad;
	int maxLeben, aktuellesLeben, angriffswert, ruestung, magieresistenz;
	
	public Held() {
		super();
		this.name = "";
		this.typ = "";
		this.beschreibung = "";
		this.maxLeben = 1;
		this.aktuellesLeben = maxLeben;
		this.angriffswert = 0;
		this.ruestung = 0;
		this.magieresistenz = 0;
	}

	public Held(String name, String typ, String beschreibung, int maxLeben, int angriffswert, int ruestung,
			int magieresistenz) {
		super();
		this.name = name;
		this.typ = typ;
		this.beschreibung = beschreibung;
		this.maxLeben = maxLeben;
		this.aktuellesLeben = maxLeben;
		this.angriffswert = angriffswert;
		this.ruestung = ruestung;
		this.magieresistenz = magieresistenz;
	}

	public int angreifen() {
		return angriffswert;
	}
	
	public void leiden(int schaden) {
		if ((schaden-ruestung)<=0) {
			aktuellesLeben -= 1;
		}else aktuellesLeben -= (schaden-ruestung);
	}
	
	public void heilen() {
		setAktuellesLeben(maxLeben);
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getTyp() {
		return typ;
	}

	public void setTyp(String typ) {
		this.typ = typ;
	}

	public String getBeschreibung() {
		return beschreibung;
	}

	public void setBeschreibung(String beschreibung) {
		this.beschreibung = beschreibung;
	}

	public String getBildpfad() {
		return bildpfad;
	}

	public void setBildpfad(String bildpfad) {
		this.bildpfad = bildpfad;
	}

	public int getMaxLeben() {
		return maxLeben;
	}

	public void setMaxLeben(int maxLeben) {
		this.maxLeben = maxLeben;
	}

	public int getAktuellesLeben() {
		return aktuellesLeben;
	}

	public void setAktuellesLeben(int aktuellesLeben) {
		this.aktuellesLeben = aktuellesLeben;
	}

	public int getAngriffswert() {
		return angriffswert;
	}

	public void setAngriffswert(int angriffswert) {
		this.angriffswert = angriffswert;
	}

	public int getRuestung() {
		return ruestung;
	}

	public void setRuestung(int ruestung) {
		this.ruestung = ruestung;
	}

	public int getMagieresistenz() {
		return magieresistenz;
	}

	public void setMagieresistenz(int magieresistenz) {
		this.magieresistenz = magieresistenz;
	}
	
	
}
