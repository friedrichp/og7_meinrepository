package array_Aufgaben;

import java.util.Arrays;
import java.util.Scanner;

public class Array_Aufgaben {

	public static void main(String[] args) {
	
		int[] meinArray;
		int arrayGroesse;
		
		System.out.println("Schwierigkeit 1");
		
		System.out.print("Aufgabe 1: ");
		arrayGroesse = 20;
		meinArray = new int[arrayGroesse];
		for (int i = 0; i<arrayGroesse; i++) {
			meinArray[i] = 1;
		}
		System.out.println(Arrays.toString(meinArray));
		
		System.out.print("Aufgabe 2: ");
		arrayGroesse = 15;
		meinArray = new int[arrayGroesse];
		for (int i = 0; i<arrayGroesse; i++) {
			meinArray[i] = i;
		}
		System.out.println(Arrays.toString(meinArray));
		
		System.out.print("Aufgabe 3: ");
		arrayGroesse = 17;
		meinArray = new int[arrayGroesse];
		for (int i = 0; i<arrayGroesse; i++) {
			meinArray[i] = arrayGroesse - i - 1;
		}
		System.out.println(Arrays.toString(meinArray));
		
		System.out.println("Schwierigkeit 2");
		
		System.out.print("Aufgabe 1: ");
		arrayGroesse = 5;
		meinArray = new int[arrayGroesse];
		Scanner input = new Scanner(System.in);
		for (int i = 0; i<arrayGroesse; i++) {
			System.out.print("Bitte Zahl " + (i + 1) + " eingeben: ");
			meinArray[i] = input.nextInt();
			System.out.print("Aufgabe 1: ");
		}
		System.out.println(Arrays.toString(meinArray));
		
	}
	
}
