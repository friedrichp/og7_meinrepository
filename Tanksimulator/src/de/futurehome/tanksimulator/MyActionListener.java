package de.futurehome.tanksimulator;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyActionListener implements ActionListener {
	public TankSimulator f;

	public MyActionListener(TankSimulator f) {
		this.f = f;
	}

	public void actionPerformed(ActionEvent e) {
		Object obj = e.getSource();
		if (obj == f.btnBeenden)
			System.exit(0);
		
		if (obj == f.btnEinfuellen) {
			 double fuellstand = f.myTank.getFuellstand();
			 fuellstand = fuellstand + 5;
			 if (fuellstand>f.myTank.getMaxFuellstand()) {
				 fuellstand = f.myTank.getMaxFuellstand();
			 }
			 
			 f.myTank.setFuellstand(fuellstand);

			 f.lblFuellstand.setText((fuellstand/f.myTank.getMaxFuellstand()*100) + "%");
		}
		if (obj == f.btnVerbrauchen) {
			double fuellstand = f.myTank.getFuellstand();
			fuellstand = fuellstand - 2;
			if (fuellstand < 0) {
				fuellstand = 0;
			}
			f.myTank.setFuellstand(fuellstand);

			f.lblFuellstand.setText((fuellstand/f.myTank.getMaxFuellstand()*100) + "%");
		}
		if (obj == f.btnZuruecksetzen) {
			f.myTank.setFuellstand(0);
			
			f.lblFuellstand.setText(0.0 + "%");
		}
	}
}
