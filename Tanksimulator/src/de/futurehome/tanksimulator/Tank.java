package de.futurehome.tanksimulator;
public class Tank {
	
	private double fuellstand;
	private double maxFuellstand;

	public Tank(double fuellstand) {
		this.fuellstand = fuellstand;
		this.maxFuellstand = 100;
	}

	public double getFuellstand() {
		return fuellstand;
	}

	public void setFuellstand(double fuellstand) {
		this.fuellstand = fuellstand;
	}

	public double getMaxFuellstand() {
		return maxFuellstand;
	}

}