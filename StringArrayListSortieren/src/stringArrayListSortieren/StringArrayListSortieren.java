package stringArrayListSortieren;

import java.util.ArrayList;
import java.util.Scanner;

public class StringArrayListSortieren {

	public static void main(String[] args) {
		ArrayList<String> meineArrayList = new ArrayList<String>();
		Scanner input = new Scanner(System.in);
		
		boolean end = false;
		do {
			String eingabe;
			eingabe = input.next();
			if (eingabe.equals("***")) {
				end = true;
			}else meineArrayList.add(eingabe);
		}while (end==false);
		
		boolean getauscht = false;
		do {
			getauscht = false;
			for (int i = 0; i < meineArrayList.size()-1; i++) {
				if (meineArrayList.get(i).compareTo(meineArrayList.get(i+1))>0) {
					//meineArrayList.sort();
					String zwischenspeicher = meineArrayList.get(i);
					meineArrayList.set(i, meineArrayList.get(i+1));
					meineArrayList.set(i+1, zwischenspeicher);
					getauscht = true;
				}
			}
		}while (getauscht == true);
		
		System.out.println(meineArrayList.toString());
	}
}
