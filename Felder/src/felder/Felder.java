package felder;

import java.lang.reflect.Array;
import java.util.Arrays;

/**
  *
  * Übungsklasse zu Feldern
  *
  * @version 1.0 vom 05.05.2011
  * @author Tenbusch
  */

public class Felder {

  //unsere Zahlenliste zum Ausprobieren
  private int[] zahlenliste = {5,8,4,3,9,1,2,7,6,0};
  
  //Konstruktor
  public Felder(){}

  //Methode die Sie implementieren sollen
  //ersetzen Sie den Befehl return 0 durch return ihre_Variable
  
  //die Methode soll die größte Zahl der Liste zurückgeben
  public int maxElement(){
	  int max = 0;
	  for (int i = 0; i < zahlenliste.length; i++) {
		  if (max < zahlenliste[i]) {
			  max = zahlenliste[i];
		  }
	  }
	  return max;
  }

  //die Methode soll die kleinste Zahl der Liste zurückgeben
  public int minElement(){
	  int min = 0;
	  for (int i = 0; i < zahlenliste.length; i++) {
		  if (min > zahlenliste[i]) {
			  min = zahlenliste[i];
		  }
	  }
	  return min;
  }
  
  //die Methode soll den abgerundeten Durchschnitt aller Zahlen zurückgeben
  public int durchschnitt(){
	  int summeDerZahlen = 0;
	  for (int i = 0; i < zahlenliste.length; i++) {
		  summeDerZahlen = summeDerZahlen + zahlenliste[i];
	  }
	  return (summeDerZahlen / zahlenliste.length);
  }

  //die Methode soll die Anzahl der Elemente zurückgeben
  //der Befehl zahlenliste.length; könnte hierbei hilfreich sein
  public int anzahlElemente(){
    return zahlenliste.length;
  }

  //die Methode soll die Liste ausgeben
  public String toString(){
    return Arrays.toString(zahlenliste);
  }

  //die Methode soll einen booleschen Wert zurückgeben, ob der Parameter in
  //dem Feld vorhanden ist
  public boolean istElement(int zahl){
	  for (int i = 0; i < zahlenliste.length; i++) {
		  if (zahl == zahlenliste[i])
			  return true;
	  }
	  return false;
  }
  
  //die Methode soll das erste Vorkommen der
  //als Parameter übergebenen  Zahl liefern oder -1 bei nicht vorhanden
  public int getErstePosition(int zahl){
	  for (int i = 0; i < zahlenliste.length; i++) {
		  if (zahl == zahlenliste[i])
			  return i;
	  }
	  return -1;
  }
  
  //die Methode soll die Liste aufsteigend sortieren
  //googlen sie mal nach Array.sort() ;)							<-- Googlen Sie mal DuckDuckGo, Startpage und Ecosia ;)
  public void sortiere(){
	  Arrays.sort(zahlenliste);
  }

  public static void main(String[] args) {
    Felder testenMeinerLösung = new Felder();
    System.out.println(testenMeinerLösung.maxElement());
    System.out.println(testenMeinerLösung.minElement());
    System.out.println(testenMeinerLösung.durchschnitt());
    System.out.println(testenMeinerLösung.anzahlElemente());
    System.out.println(testenMeinerLösung.toString());
    System.out.println(testenMeinerLösung.istElement(9));
    System.out.println(testenMeinerLösung.getErstePosition(5));
    testenMeinerLösung.sortiere();
    System.out.println(testenMeinerLösung.toString());
  }
}
